# fpcut

A utility for easily opening system-installed binaries in flatpaks by making scripts to open them.

## Installation

Install it by running `curl https://gitlab.com/VarLad/fpcut/-/raw/main/fpcut -o ~/.local/bin/fpcut && chmod +x ~/.local/bin/fpcut`

## Usage

Run `fpcut -h` to view options related to usage
